import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from "@angular/material/icon";

import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { Loader } from "@googlemaps/js-api-loader"
import { google } from "google-maps";
import * as Mapboxgl from 'mapbox-gl';
import { Scooter } from 'app/model/scooter';
import { ScooterService } from 'app/service/scooter.service';

import { Observable, Subject, Subscription, timer } from 'rxjs';
import { startWith, switchMap  } from 'rxjs/operators';
import { Ride } from 'app/model/rides';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { RideService } from 'app/service/ride.service';
import MarkerClusterer from '@googlemaps/markerclustererplus';
import { UserService } from 'app/service/user.service';
import * as globals from '../../../global';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit{

  scooters: Scooter[] | undefined;
  map: google.maps.Map | undefined;
  markers: google.maps.Marker[] = [];

  nrCustomers: number | undefined;
  nrScooters: number | undefined;
  nrRides: number | undefined;
  revenue: any;
  stats: any;

  loader = new Loader({
    apiKey: globals.maps,
  });


  rides: Ride[] | undefined;
  dataSource: any;

  displayedColumns = ['date','duration', 'distance', 'scooter', 'status', 'cost', 'action'];

  reset = new Subject();
  timer: Observable<any>;

  constructor(private scooterService: ScooterService, private rideService: RideService, 
    private userService: UserService){
    this.timer = this.reset.pipe(
      startWith(0),
      switchMap(() => timer(0, 30000))
    );
  }

  ngOnInit(): void {

    this.loadData();

    this.loader.load().then(() => {
      this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: { lat: 46.768212 , lng: 23.588601 },
        zoom: 13,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: false,
        styles: [
          {
              featureType: "poi",
              elementType: "labels",
              stylers: [
                    { visibility: "off" }
              ]
          }
      ]
        
      });
      this.getScooters();

    });

    this.resetTimer();

  }

  @ViewChild(MatSort, {static: false}) set sort(value: MatSort){
    if(this.dataSource){
      
      this.dataSource.sort = value;
    }
  };

  async loadData(){
    this.rides = await this.rideService.getRidesList();
    this.rides.map(ride=>{
      const end = new Date(ride.endTime).getTime();
      const start = new Date(ride.startTime).getTime();
      ride.duration = Math.round((end - start)/1000);
    })
    this.dataSource = new MatTableDataSource(this.rides.slice(Math.max(this.rides.length - 4, 0)).filter(ride => {
      if(ride.status === 'completed')
        return ride;
      else return 
    }));
    this.nrCustomers = await this.userService.countCustomers();
    this.nrScooters = await this.scooterService.countScooters();
    this.nrRides = await this.rideService.countRides();
    this.revenue = await this.rideService.getRevenue();

    this.stats = await this.rideService.getStats();
    this.lineChartData = [
      { data: this.stats.revenue },
    ];
    this.lineChartLabels = this.stats.month;

    console.log(this.stats);
  }


  transform(value: number): string {
    const minutes: number = Math.floor((value % 3600)/60);
    const hours: number = Math.floor(value / 3600);
    return ( (hours == 0 && minutes == 0) ? (value % 60) + 's' : (hours == 0) ? minutes + 'm' + (value % 60) +'s' : hours + 'h' + minutes + 'm' + (value % 60)+'s') ;
 }


  resetTimer(){
    const subscription = this.timer.subscribe((i) => {
      console.log('Reloaded');
      if(i === 2){
        this.reset.next(void 0);
        this.deleteMarkers();
        this.getScooters();
        // this.map?.setZoom(this.map.getZoom());
      }
  });
  }

  async getScooters(){
    this.scooters = await this.scooterService.getAllScooters();
    this.scooters.forEach(scooter => {
      if(scooter.status === "available"){
        this.createMarker(scooter);
      }
    })
    console.log(this.markers);
    if(this.map){
      const cluster = new MarkerClusterer(this.map, this.markers, {
        imagePath:
          "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
      });
    }

  }

  createMarker(scooter: Scooter){
    const infowindow = new google.maps.InfoWindow({
      content: '<p>ScooterID: '+scooter.idScooter+'</p>'+'<p>Battery:'+scooter.battery+'</p>'+`<a href="/scooters/${scooter._id}">View scooter</a>`
    });

    const marker = new google.maps.Marker({
      position: { lat: scooter.location.coordinates[0], lng: 	scooter.location.coordinates[1] },
      icon: "https://i.ibb.co/8DGqKsR/scooter-pin.png",
      map: this.map,
    });

    this.markers.push(marker);
    
    marker.addListener("click", () => {
      infowindow.open(this.map, marker);
    });
  
  }

  deleteMarkers() {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  //Chart
  lineChartData: ChartDataSets[] = [];

  lineChartLabels: Label[] = [];

  public lineChartOptions: ChartOptions = {
    responsive: true,
  };

  public lineChartColors: Color[] = [
    {
      borderColor: 'rgba(59, 17, 89, 0.8)',
      backgroundColor: 'rgba(59, 17, 89, 0.4)',
    },
  ];

  public lineChartLegend = false;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

}
