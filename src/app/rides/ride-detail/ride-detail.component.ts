import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute } from '@angular/router';
import { Ride } from 'app/model/rides';
import { RideService } from 'app/service/ride.service';
import { Loader } from "@googlemaps/js-api-loader"
import * as globals from '../../../../global';

@Component({
  selector: 'app-ride-detail',
  templateUrl: './ride-detail.component.html',
  styleUrls: ['./ride-detail.component.scss']
})
export class RideDetailComponent implements OnInit {


  username: string | undefined;
  rideID: string | null | undefined;
  ride: Ride;
  polyline: {lat: number, lng: number}[] = [];

  origin: any;
  destination: any;

  map: google.maps.Map | undefined;
  loader = new Loader({
    apiKey: globals.maps,
  });

  constructor(private rideService: RideService, private activatedRoute: ActivatedRoute) {
   }

  ngOnInit(): void {
    this.rideID = this.activatedRoute.snapshot.paramMap.get('id');
    this.rideService.getRideDetails(this.rideID).subscribe(ride => {
      this.ride = ride
      const end = new Date(ride.endTime).getTime();
      const start = new Date(ride.startTime).getTime();
      ride.duration = Math.round((end - start)/1000);

      console.log(this.ride);
      
      this.loadAddress(this.ride);

      this.loader.load().then(() => {

        const bounds = new google.maps.LatLngBounds();

        if(this.ride.route){
          for(var i = 0; i<this.ride.route.length; i++){
            this.polyline.push({lat: this.ride.route[i].coordinates[0], lng: this.ride.route[i].coordinates[1]});
            bounds.extend(new google.maps.LatLng(this.ride.route[i].coordinates[0], this.ride.route[i].coordinates[1]));
          }
        }
        const center = bounds?.getCenter();
        
        this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
          // center: { lat: 46.768212 , lng: 23.588601 },
          center: center,
          zoom: 10,
          mapTypeControl: false,
          streetViewControl: false,
          zoomControl: false,
          scrollwheel: false,
          draggable: false,
          fullscreenControl: false,
          styles: [
            {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                      { visibility: "off" }
                ]
            }
        ]
        });
        this.map.fitBounds(bounds);

        const path = new google.maps.Polyline({
          path: this.polyline,
          geodesic: true,
          strokeColor: "#3B1159",
          strokeOpacity: 1.0,
          strokeWeight: 3,
        });
      
        path.setMap(this.map);

    }
    );
    // this.loadDetails();

    
    });
    
  }

  transform(value: number): string {
    const minutes: number = Math.floor((value % 3600)/60);
    const hours: number = Math.floor(value / 3600);
    return ( (hours == 0 && minutes == 0) ? (value % 60) + 's' : (hours == 0) ? minutes + 'm' + (value % 60) +'s' : hours + 'h' + minutes + 'm' + (value % 60)+'s') ;
 }

  async loadAddress(ride: Ride){
    console.log("Origin: " + ride.route[0].coordinates);
    const address = await this.rideService.getAddress(ride.route[0].coordinates, ride.route[ride.route.length-1].coordinates)
    this.origin = address.origin.split(',')[0];
    this.destination = address.destination.split(',')[0];
  }


}
