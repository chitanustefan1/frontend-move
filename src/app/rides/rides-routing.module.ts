import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RideDetailComponent } from './ride-detail/ride-detail.component';
import { RidesComponent } from './rides.component';

const routes: Routes = [
  { path: '', component: RidesComponent },
  { path: ':id', component: RideDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RidesRoutingModule { }
