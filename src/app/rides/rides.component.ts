import { Component, OnInit, ViewChild } from '@angular/core';
import { Ride } from 'app/model/rides';
import { RideService } from 'app/service/ride.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, MatSortable} from '@angular/material/sort';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Loader } from "@googlemaps/js-api-loader"

@Component({
  selector: 'app-rides',
  templateUrl: './rides.component.html',
  styleUrls: ['./rides.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class RidesComponent implements OnInit {

  rides: Ride[] | undefined;
  isExpanded = false;
  dataSource: any;

  displayedColumns = ['date', 'user', 'duration', 'distance', 'scooter', 'status', 'cost', 'action'];
  
  expandedElement: any | null | undefined;
  
  constructor(private rideService: RideService) { }


  ngOnInit(){
    this.loadData();
  }

  async loadData(){
    this.rides = await this.rideService.getRidesList();
    console.log(this.rides);
    this.rides.map(ride=>{
      const end = new Date(ride.endTime).getTime();
      const start = new Date(ride.startTime).getTime();
      ride.duration = Math.round((end - start)/1000);
    })
    this.dataSource = new MatTableDataSource(this.rides);
    // console.log(this.scooters);
  }

  transform(value: number): string {
    if(value){
      const minutes: number = Math.floor((value % 3600)/60);
      const hours: number = Math.floor(value / 3600);
      return ( (hours == 0 && minutes == 0) ? (value % 60) + 's' : (hours == 0) ? minutes + 'm' + (value % 60) +'s' : hours + 'h' + minutes + 'm' + (value % 60)+'s') ;
    }else{
      return 'null';
    }

 }

  @ViewChild(MatSort, {static: false}) set sort(value: MatSort){
    if(this.dataSource){
      
      this.dataSource.sort = value;
    }
  };

  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    if (this.dataSource){
      this.dataSource.paginator = value;
      this.dataSource.paginator.pageSize = 10;
    }
  }
}
