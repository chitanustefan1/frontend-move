import { Injectable } from '@angular/core';
import { Scooter } from '../model/scooter';
import { HttpClient } from '@angular/common/http';
import * as globals from '../../../global';

@Injectable({
  providedIn: 'root'
})
export class ScooterService {

  constructor(private http: HttpClient) { }

  async getScootersList(){
    return this.http.get<Scooter[]>(globals.baseUrl + '/admin/scooters/').toPromise();
  }

  async getScooterByID(id: string | null | undefined){
    return this.http.get<Scooter>(globals.baseUrl + `/admin/scooters/${id}`).toPromise();
  }

  async getAllScooters(){
    return this.http.get<Scooter[]>(globals.baseUrl + `/admin/scooters/`).toPromise();
  }

  addScooter(idScooter: string, battery: number, serialNumber: string, lat: number, lng: number){
    return this.http.post<Scooter>(globals.baseUrl + `/admin/scooters/add`, {idScooter: idScooter, battery: battery, serialNumber: serialNumber, lat: lat, lng: lng});
  }

  suspendScooter(id: string, status: string){
    return this.http.put(globals.baseUrl + `/admin/scooters/suspend/${id}`, {status: status});
  }

  async countScooters(){
    return this.http.get<number>(globals.baseUrl + `/admin/dashboard/scooters`).toPromise();
  }

  async getAddress(lat: number, long: number){
    console.log(encodeURIComponent(JSON.stringify(origin)));
    return this.http.get<any>(globals.baseUrl + `/api/scooter/location?lat=${encodeURIComponent(JSON.stringify(lat))}&long=${encodeURIComponent(JSON.stringify(long))}`).toPromise();
  }

  

}
