import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ride } from '../model/rides'; 
import * as globals from '../../../global';

@Injectable({
  providedIn: 'root'
})
export class RideService {

  constructor(private http: HttpClient) { }

  async getRidesList(){
    return this.http.get<Ride[]>(globals.baseUrl + '/admin/rides/').toPromise();
  }

  public getRideDetails(id: string | null | undefined){
    return this.http.get<Ride>(globals.baseUrl + `/admin/rides/${id}`);
  }

  async getUsername(id: string | null | undefined){
    return this.http.get<string>(globals.baseUrl + `/admin/rides/username/${id}`).toPromise();
  }

  async getAddress(origin: number[], destination: number[]){
    console.log(encodeURIComponent(JSON.stringify(origin)));
    return this.http.get<any>(globals.baseUrl + `/api/ride/address?origin=${encodeURIComponent(JSON.stringify(origin))}&destination=${encodeURIComponent(JSON.stringify(destination))}`).toPromise();
  }

  async countRides(){
    return this.http.get<number>(globals.baseUrl + `/admin/dashboard/rides`).toPromise();
  }

  async getRevenue(){
    return this.http.get(globals.baseUrl + `/admin/dashboard/revenue`).toPromise();
  }

  async getStats(){
    return this.http.get(globals.baseUrl + `/admin/dashboard/stats`).toPromise();
  }


}
