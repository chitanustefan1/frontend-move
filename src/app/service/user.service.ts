import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user';
import * as globals from '../../../global';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http: HttpClient) { }

  getCustomerList(){
    return this.http.get<User[]>(globals.baseUrl + '/admin/customers').toPromise();
  }

  login(email: string, password: string){
    return this.http.post<User>(globals.baseUrl + '/api/user/login', {email: email, password: password});
  }

  getLicense(id: string){
    return this.http.get(globals.baseUrl + `/admin/customers/license/${id}`).toPromise();
  }

  suspendUser(id: string, status: string){
    return this.http.put(globals.baseUrl + `/admin/customers/suspend/${id}`, {status: status});
  }

  async countCustomers(){
    return this.http.get<number>(globals.baseUrl + `/admin/dashboard/customers`).toPromise();
  }

}

