interface Coordinates {
    type: string;
    coordinates: number[];
}

export interface Scooter{
    _id?: string,
    idScooter: string,
    battery: number,
    serialNumber: string,
    location: Coordinates,
    status: string,
}