interface Coordinates {
    type: string;
    coordinates: number[];
}

export interface Ride{
    _id: string,
    startTime: Date,
    endTime: Date,
    scooter: string,
    scooterID: string,
    userID: string,
    userEmail: string,
    route: Coordinates[];
    distance: number,
    duration: number,
    status: string,
    cost: number
}