export interface User{
    id: string,
    username: string,
    email: string,
    role: string,
    token: string,
    status: string,
    registeredAt: Date;
}