import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontAngular';
  isExpanded = false;
  showHeader = true;
  router;
  constructor(private _router: Router ) {
    this.router = _router;
  }

  modifyHeader() { // This method is called many times
    console.log(this.router.url); // This prints a loot of routes on console
    if (this.router.url === '/login') {
        this.showHeader = false;
    } else {
        this.showHeader = true;
    }
}
}
