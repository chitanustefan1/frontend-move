import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { User } from 'app/model/user';
import { UserService } from 'app/service/user.service';

@Component({
  selector: 'app-viewlicense',
  templateUrl: './viewlicense.component.html',
  styleUrls: ['./viewlicense.component.scss']
})
export class ViewlicenseComponent implements OnInit {

  licensePic: string | undefined;

  constructor(public dialogRef: MatDialogRef<ViewlicenseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private userService: UserService) { }

  ngOnInit(): void {
    console.log(this.data)
    this.getPicture();
  }

  async getPicture(){
    const link = await this.userService.getLicense(this.data.id);
    this.licensePic = link.toString();
    console.log(this.licensePic);
  }

  suspendUser(){
    if(this.data.status === "active"){
      this.userService.suspendUser(this.data.id, "suspended").subscribe(res => {
        console.log(res);
      });
      
    }else{
      this.userService.suspendUser(this.data.id, "active").subscribe(res => {
        console.log(res);
      });
    }
  }

}
