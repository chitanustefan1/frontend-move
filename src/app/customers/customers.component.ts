import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { User } from '../model/user';
import { UserService } from '../service/user.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewlicenseComponent } from './viewlicense/viewlicense.component';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  isExpanded = false;
  dataSource: any;
  customers: User[] | undefined;
  displayedColumns: string[] = ['id', 'username', 'email', 'status', 'last registered', 'action'];

  constructor(private userService: UserService, public dialog: MatDialog){}

  ngOnInit(){
    this.loadData();
  }

  async loadData(){
    this.customers = await this.userService.getCustomerList();
    this.dataSource = new MatTableDataSource(this.customers);
    console.log(this.customers);
  }

  openDialog(userId: string, email: string, status: String): void {
    const dialogRef = this.dialog.open(ViewlicenseComponent, {
      width: '450px',
      data: {id: userId, email: email, status: status}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  @ViewChild(MatSort, {static: false}) set sort(value: MatSort){
    if(this.dataSource){
      this.dataSource.sort = value;
    }
    this.dataSource.sortingDataAccessor = (item: { [x: string]: any; fromDate: string | number | Date; }, property: string | number) => {
      switch (property) {
        case 'fromDate': return new Date(item.fromDate);
        default: return item[property];
      }
    };
  };

  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    if (this.dataSource){
      this.dataSource.paginator = value;
      this.dataSource.paginator.pageSize = 10;
    }
  }

}
