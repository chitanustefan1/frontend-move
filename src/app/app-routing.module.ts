import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthguardGuard } from './authguard.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'dashboard', canActivate:[AuthguardGuard], component: DashboardComponent},
  { path: 'customers', canActivate:[AuthguardGuard], loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule) },
  { path: 'scooters', canActivate:[AuthguardGuard], loadChildren: () => import('./scooter/scooter.module').then(m => m.ScooterModule) },
  { path: 'rides', canActivate:[AuthguardGuard], loadChildren: () => import('./rides/rides.module').then(m => m.RidesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
