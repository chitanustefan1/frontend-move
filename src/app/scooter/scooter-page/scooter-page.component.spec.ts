import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScooterPageComponent } from './scooter-page.component';

describe('ScooterPageComponent', () => {
  let component: ScooterPageComponent;
  let fixture: ComponentFixture<ScooterPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScooterPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScooterPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
