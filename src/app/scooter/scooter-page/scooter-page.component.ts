import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Scooter } from 'app/model/scooter';
import { ScooterService } from 'app/service/scooter.service';
import { Loader } from "@googlemaps/js-api-loader"
import * as globals from '../../../../global';

@Component({
  selector: 'app-scooter-page',
  templateUrl: './scooter-page.component.html',
  styleUrls: ['./scooter-page.component.scss']
})
export class ScooterPageComponent implements OnInit {


  scooterID: string | null | undefined;
  scooter: Scooter | undefined;


  map: google.maps.Map | undefined;
  loader = new Loader({
    apiKey: globals.maps,
  });


  constructor(private scooterService: ScooterService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.scooterID = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.scooterID);
    this.loadScooter();

    this.loader.load().then(() => {

      if(this.scooter){
        this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
          center: { lat: this.scooter.location.coordinates[0] , lng: this.scooter.location.coordinates[1] },
          zoom: 16,
          mapTypeControl: false,
          streetViewControl: false,
          zoomControl: false,
          styles: [
            {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                      { visibility: "off" }
                ]
            }
        ]
        });

        const marker = new google.maps.Marker({
          position: { lat: this.scooter.location.coordinates[0], lng: 	this.scooter.location.coordinates[1] },
          map: this.map,
          });
      }  
    });
  
  }

  async loadScooter(){
    this.scooter = await this.scooterService.getScooterByID(this.scooterID);
  }

  changeStatus(status: string){
    if(this.scooterID){
      this.scooterService.suspendScooter(this.scooterID, status).subscribe(res=>{
        console.log(res);
      })
      window.location.reload()
    }
    }


}
