import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {ScooterService} from '../service/scooter.service';
import {Scooter} from '../model/scooter';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AddScooterComponent } from './add-scooter/add-scooter.component';

@Component({
  selector: 'app-scooter',
  templateUrl: './scooter.component.html',
  styleUrls: ['./scooter.component.scss']
})
export class ScooterComponent implements OnInit {

  scooters: Scooter[] | undefined;
  isExpanded = false;
  dataSource: any;

  displayedColumns: string[] = ['id', 'location', 'battery', 'status', 'action'];
  
  totalScooters: number | undefined;
  
  constructor(private scooterService: ScooterService, public dialog: MatDialog) { }


  ngOnInit(){
    this.loadData();
    this.totalScooters = this.scooters?.length;
  }

  async loadData(){
    this.scooters = await this.scooterService.getScootersList();
    this.dataSource = new MatTableDataSource(this.scooters);
    // console.log(this.scooters);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddScooterComponent, {
      width: '400px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  suspendScooter(id: string, status: string){
    this.scooterService.suspendScooter(id, status).subscribe(res=>{
      console.log(res);
    })
    window.location.reload()
  }

  @ViewChild(MatSort, {static: false}) set sort(value: MatSort){
    if(this.dataSource){
      this.dataSource.sort = value;
    }
  };

  @ViewChild(MatPaginator, {static: false})
  set paginator(value: MatPaginator) {
    if (this.dataSource){
      this.dataSource.paginator = value;
      this.dataSource.paginator.pageSize = 10;
    }
  }
}
