import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScooterPageComponent } from './scooter-page/scooter-page.component';
import { ScooterComponent } from './scooter.component';

const routes: Routes = [
  { path: '', component: ScooterComponent },
  { path: ':id', component: ScooterPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScooterRoutingModule { }
