import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddScooterComponent } from './add-scooter.component';

describe('AddScooterComponent', () => {
  let component: AddScooterComponent;
  let fixture: ComponentFixture<AddScooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddScooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
