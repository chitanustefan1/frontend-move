import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';
import { Loader } from "@googlemaps/js-api-loader"
import { ScooterService } from 'app/service/scooter.service';
import { Scooter } from 'app/model/scooter';

// import { randomBytes } from 'crypto';


@Component({
  selector: 'app-add-scooter',
  templateUrl: './add-scooter.component.html',
  styleUrls: ['./add-scooter.component.scss']
})
export class AddScooterComponent implements OnInit {


  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(4),
  ]);

  serialFormControl = new FormControl('', [
    Validators.required,
  ]);

  scooterID: string = "";
  serialNumber: string = "";


  map: google.maps.Map | undefined;
  markers: google.maps.Marker[] = [];

  lat: number = 0;
  lng: number = 0;
  battery: number = 100;

  create: boolean = false;

  loader = new Loader({
    apiKey: "AIzaSyCYJmhE9MlZHwtziptiZ6ywcQvCuum2p3k",
  });

  constructor(
    public dialogRef: MatDialogRef<AddScooterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private scooterService: ScooterService
  ) { }

  ngOnInit(): void {

    this.loader.load().then(() => {
      this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: { lat: 46.768212 , lng: 23.588601 },
        zoom: 13,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: false,
        styles: [
          {
              featureType: "poi",
              elementType: "labels",
              stylers: [
                    { visibility: "off" }
              ]
          }
      ]
      });

      var myLatlng = new google.maps.LatLng(46.768212, 23.588601);

      const marker = new google.maps.Marker({
        position: myLatlng,
        map: this.map,
        title: "Your location",
      });
    
      google.maps.event.addListener(this.map, 'click', (event) => {
        this.lat = event.latLng.lat();
        this.lng = event.latLng.lng();
        marker.setPosition(event.latLng);
      });

    });

  }

  generateID(parts: number, type: string){
    const stringArr = [];
    for(let i = 0; i< parts; i++){
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      stringArr.push(S4);
    }
    if(type === 'idScooter'){
      this.scooterID = stringArr[0];
    }else{
      this.serialNumber = stringArr.join('-');
    }
  }

  submit(){
    if(this.create) {
      this.scooterService.addScooter(this.scooterID, this.battery, this.serialNumber, this.lat, this.lng).subscribe(scooter => {
        console.log(scooter);
      });
    }
  }

  createClick(){
    this.create = true;
  }

}
