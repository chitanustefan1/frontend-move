import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScooterRoutingModule } from './scooter-routing.module';
import { ScooterComponent } from './scooter.component';

import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatChipsModule} from '@angular/material/chips';
import {MatSortModule} from '@angular/material/sort';

import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { ScooterPageComponent } from './scooter-page/scooter-page.component';
import { AddScooterComponent } from './add-scooter/add-scooter.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSliderModule} from '@angular/material/slider';
import {MatMenuModule} from '@angular/material/menu';


@NgModule({
  declarations: [
    ScooterComponent,
    ScooterPageComponent,
    AddScooterComponent
  ],
  imports: [
    CommonModule,
    ScooterRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatChipsModule,
    MatSortModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatMenuModule
  ]
})
export class ScooterModule { }
