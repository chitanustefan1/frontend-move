import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'app/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  emailV = new FormControl('', [Validators.required, Validators.email]);
  passwordV = new FormControl('', Validators.required);
  hide = true;
  checked = false;

  email: string = '';
  password: string = '';

  error: boolean | undefined;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    
  }

  login(){
    console.log(this.email);
    console.log(this.password);
    this.userService.login(this.email, this.password).subscribe(
      response => {
        if(response.role === "admin"){
          localStorage.setItem('token', response.token);
          this.router.navigateByUrl('/dashboard');
        }else{
          this.error = true;
        }
        
      }
    );
  }


  getErrorMessage(error: string) {
    switch (error) {
      case 'email':
        if (this.emailV.hasError('required')) {
          return 'Email required';
        }
        if(this.emailV.hasError('email')){
          return 'Not a valid email';
        }
        break;
      case 'password':
        if (this.passwordV.hasError('required')) {
          return 'Password required';
        }
        break;
      default: return '';
    }
    return '';
  }


}
